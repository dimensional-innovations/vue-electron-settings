
/**
 * Represents the config values that can be set to change how the app behaves.
 */
export interface AppConfig extends Record<string, number | string | boolean> { }