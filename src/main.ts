import * as settings from 'electron-settings';
import { app, ipcMain, IpcMainInvokeEvent } from 'electron';
import * as os from 'os';
import { AppConfig } from './AppConfig';

/**
 * Initializes the handlers used to manage settings.
 * 
 * @param config - The default app config to use. If this is the is the first time the app 
 *                 has been ran, then these values are used. Otherwise these values are used 
 *                 whenever the settings are reset.
 * @returns - The current value for the app config.
 */
export async function initSettings<T = AppConfig>(config: T): Promise<T> {
  const schema =  Object.entries(config)
    .reduce((schema, [key, value]) => {
      schema[key] = typeof value;
      return schema;
    }, {} as Record<string, 'string' | 'number' | 'bigint' | 'boolean' | 'symbol' | 'undefined' | 'object' | 'function'>);

  if (!(await settings.has('config'))) {
    await settings.set('config', config as any);
  }
  else {
    for (let [key, value] of Object.entries(config)) {
      if (!(await settings.has(`config.${key}`))) {
        await settings.set(`config.${key}`, value as any);
      }
    }
  }

  ipcMain.handle('settings:config', accessAppConfig());
  ipcMain.handle('settings:config:reset', resetAppConfig(config))
  ipcMain.handle('settings:config:schema', () => schema);

  ipcMain.handle('settings:paths', getAppPaths);
  ipcMain.handle('settings:os', getOsInfo);
  ipcMain.handle('settings:ip', getIpConfig);

  return await settings.get('config') as any;
}

/**
 * Allows the renderer to get or set the app config.
 */
function accessAppConfig() {
  return async (_: IpcMainInvokeEvent, config: any) => {
    if (config) {
      await settings.set('config', config);
    }
    return settings.get('config');
  };
}

/**
 * Resets the app config to the provided default values.
 * 
 * @param config - The default value to use for app config.
 */
function resetAppConfig(config: any) {
  return async () => {
    await settings.set('config', config);
    return await settings.get('config');
  }
}

/**
 * Returns relevant app paths as an object, with the path name as the key and
 * the absolute path as the value.
 */
function getAppPaths() {
  const pathNames = ['appData', 'userData', 'temp', 'logs'] as const;
  return Object.fromEntries(pathNames.map(name => [name, app.getPath(name)]));
}

/**
 * Returns relevant information about the operating system.
 */
function getOsInfo() {
  return {
    hostname: os.hostname(),
    uptime: os.uptime()
  };
}

/**
 * Returns information about available network interfaces.
 */
function getIpConfig() {
  return Object.values(os.networkInterfaces())
    .filter((ifaces: Array<os.NetworkInterfaceInfo> | undefined): ifaces is Array<os.NetworkInterfaceInfo> => !!ifaces)
    .reduce((a, ifaces) => a.concat(ifaces), [] as Array<os.NetworkInterfaceInfo>)
    .find(iface => iface.family === 'IPv4' && !iface.internal);
}