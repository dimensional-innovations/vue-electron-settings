import { inject, Plugin } from 'vue'; 
import { AppConfig } from './AppConfig';

const key = Symbol('settings');

/**
 * Generates the plugin to provide settings.
 * 
 * @param factory - The settings to provide. 
 * @returns - The plugin to use with Vue.use
 */
export function createSettings<T extends AppConfig = AppConfig>(factory: T | (() => T)): Plugin {
  return {
    install(app) {
      const _settings = typeof factory === 'function'
        ? (factory as any)()
        : factory;

      app.provide(key, _settings);
      app.config.globalProperties.$settings = _settings;
    }
  };
}

/**
 * Get the currently register app settings.
 * 
 * @returns - App config values.
 */
export function useSettings<T extends AppConfig = AppConfig>(): T {
  const _settings = inject<T>(key);
  if (!_settings) {
    throw new Error('Settings not found. Make sure the settings plugin is installed.');
  }

  return _settings;
}

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $settings: AppConfig;
  }
}
