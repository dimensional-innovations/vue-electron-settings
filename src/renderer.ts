import type { IpcRenderer } from 'electron';
import type { AppConfig } from './AppConfig';

function getIpcRenderer(): IpcRenderer {
  const ipcRenderer = window?.require('electron').ipcRenderer;
  if (!ipcRenderer) {
    throw new Error('provide IpcRenderer module or enable nodeIntegration');
  }

  return ipcRenderer;
}


/**
 * Get the current application config values.
 * 
 * @returns - A promise that resolves with the application config values.
 */
export async function getAppConfig<T = AppConfig>(_ipcRenderer?: IpcRenderer): Promise<T> {
  const ipcRenderer = _ipcRenderer || getIpcRenderer();
  return await ipcRenderer.invoke('settings:config');
}

/**
 * Updates the current application config values.
 * 
 * @param config - The new config value to set.
 * @returns - A promise that resolves with the application config values.
 */
export async function setAppConfig<T = AppConfig>(config: T, _ipcRenderer?: IpcRenderer): Promise<T> {
  const ipcRenderer = _ipcRenderer || getIpcRenderer();
  return await ipcRenderer.invoke('settings:config', config);
}

/**
 * Resets the application config to the default values.
 * 
 * @returns - A promise resolves with the application config values.
 */
export async function resetAppConfig<T = AppConfig>(_ipcRenderer?: IpcRenderer): Promise<T> {
  const ipcRenderer = _ipcRenderer || getIpcRenderer();
  return await ipcRenderer.invoke('settings:config:reset');
}

/**
 * Gets the schema for the app config object, with the key being the property name on the config object,
 * and the value being the type. The schema is based on the current default value for the config object.
 * 
 * @returns - A promise that returns with the schema for the config object.
 */
export async function getAppConfigSchema(_ipcRenderer?: IpcRenderer): Promise<Record<string, 'string' | 'number' | 'bigint' | 'boolean' | 'symbol' | 'undefined' | 'object' | 'function'>> {
  const ipcRenderer = _ipcRenderer || getIpcRenderer();
  return await ipcRenderer.invoke('settings:config:schema');
}

/**
 * Gets relevant application paths as an object, with the path name as the key and
 * absolute path as the value. 
 * 
 * @returns - A promise that resolves with the application paths. 
 */
export async function getAppPaths(_ipcRenderer?: IpcRenderer): Promise<Record<string, string>> {
  const ipcRenderer = _ipcRenderer || getIpcRenderer();
  return await ipcRenderer.invoke('settings:paths');
}

/**
 * Gets relevant information about the current operating system.
 * 
 * @returns - A promise that resolves with an object containing details on the current os.
 */
export async function getOsInfo(_ipcRenderer?: IpcRenderer): Promise<{ hostname: string, uptime: number}> {
  const ipcRenderer = _ipcRenderer || getIpcRenderer();
  return await ipcRenderer.invoke('settings:os');
}

/**
 * Gets details about the available network interfaces.
 * 
 * @returns - A promise that resolve with the available network interfaces.
 */
export async function getIpConfig(_ipcRenderer?: IpcRenderer): Promise<any> {
  const ipcRenderer = _ipcRenderer || getIpcRenderer();
  return await ipcRenderer.invoke('settings:ip');
}