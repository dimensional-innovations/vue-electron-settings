## Functions

<dl>
<dt><a href="#initSettings">initSettings(config)</a> ⇒</dt>
<dd><p>Initializes the handlers used to manage settings.</p>
</dd>
<dt><a href="#accessAppConfig">accessAppConfig()</a></dt>
<dd><p>Allows the renderer to get or set the app config.</p>
</dd>
<dt><a href="#resetAppConfig">resetAppConfig(config)</a></dt>
<dd><p>Resets the app config to the provided default values.</p>
</dd>
<dt><a href="#getAppPaths">getAppPaths()</a></dt>
<dd><p>Returns relevant app paths as an object, with the path name as the key and
the absolute path as the value.</p>
</dd>
<dt><a href="#getOsInfo">getOsInfo()</a></dt>
<dd><p>Returns relevant information about the operating system.</p>
</dd>
<dt><a href="#getIpConfig">getIpConfig()</a></dt>
<dd><p>Returns information about available network interfaces.</p>
</dd>
<dt><a href="#createSettings">createSettings(factory)</a> ⇒</dt>
<dd><p>Generates the plugin to provide settings.</p>
</dd>
<dt><a href="#useSettings">useSettings()</a> ⇒</dt>
<dd><p>Get the currently register app settings.</p>
</dd>
<dt><a href="#getAppConfig">getAppConfig()</a> ⇒</dt>
<dd><p>Get the current application config values.</p>
</dd>
<dt><a href="#setAppConfig">setAppConfig(config)</a> ⇒</dt>
<dd><p>Updates the current application config values.</p>
</dd>
<dt><a href="#resetAppConfig">resetAppConfig()</a> ⇒</dt>
<dd><p>Resets the application config to the default values.</p>
</dd>
<dt><a href="#getAppConfigSchema">getAppConfigSchema()</a> ⇒</dt>
<dd><p>Gets the schema for the app config object, with the key being the property name on the config object,
and the value being the type. The schema is based on the current default value for the config object.</p>
</dd>
<dt><a href="#getAppPaths">getAppPaths()</a> ⇒</dt>
<dd><p>Gets relevant application paths as an object, with the path name as the key and
absolute path as the value.</p>
</dd>
<dt><a href="#getOsInfo">getOsInfo()</a> ⇒</dt>
<dd><p>Gets relevant information about the current operating system.</p>
</dd>
<dt><a href="#getIpConfig">getIpConfig()</a> ⇒</dt>
<dd><p>Gets details about the available network interfaces.</p>
</dd>
</dl>

<a name="initSettings"></a>

## initSettings(config) ⇒
Initializes the handlers used to manage settings.

**Kind**: global function  
**Returns**: - The current value for the app config.  

| Param | Description |
| --- | --- |
| config | The default app config to use. If this is the is the first time the app                 has been ran, then these values are used. Otherwise these values are used                 whenever the settings are reset. |

<a name="accessAppConfig"></a>

## accessAppConfig()
Allows the renderer to get or set the app config.

**Kind**: global function  
<a name="resetAppConfig"></a>

## resetAppConfig(config)
Resets the app config to the provided default values.

**Kind**: global function  

| Param | Description |
| --- | --- |
| config | The default value to use for app config. |

<a name="getAppPaths"></a>

## getAppPaths()
Returns relevant app paths as an object, with the path name as the key andthe absolute path as the value.

**Kind**: global function  
<a name="getOsInfo"></a>

## getOsInfo()
Returns relevant information about the operating system.

**Kind**: global function  
<a name="getIpConfig"></a>

## getIpConfig()
Returns information about available network interfaces.

**Kind**: global function  
<a name="createSettings"></a>

## createSettings(factory) ⇒
Generates the plugin to provide settings.

**Kind**: global function  
**Returns**: - The plugin to use with Vue.use  

| Param | Description |
| --- | --- |
| factory | The settings to provide. |

<a name="useSettings"></a>

## useSettings() ⇒
Get the currently register app settings.

**Kind**: global function  
**Returns**: - App config values.  
<a name="getAppConfig"></a>

## getAppConfig() ⇒
Get the current application config values.

**Kind**: global function  
**Returns**: - A promise that resolves with the application config values.  
<a name="setAppConfig"></a>

## setAppConfig(config) ⇒
Updates the current application config values.

**Kind**: global function  
**Returns**: - A promise that resolves with the application config values.  

| Param | Description |
| --- | --- |
| config | The new config value to set. |

<a name="resetAppConfig"></a>

## resetAppConfig() ⇒
Resets the application config to the default values.

**Kind**: global function  
**Returns**: - A promise resolves with the application config values.  
<a name="getAppConfigSchema"></a>

## getAppConfigSchema() ⇒
Gets the schema for the app config object, with the key being the property name on the config object,and the value being the type. The schema is based on the current default value for the config object.

**Kind**: global function  
**Returns**: - A promise that returns with the schema for the config object.  
<a name="getAppPaths"></a>

## getAppPaths() ⇒
Gets relevant application paths as an object, with the path name as the key andabsolute path as the value.

**Kind**: global function  
**Returns**: - A promise that resolves with the application paths.  
<a name="getOsInfo"></a>

## getOsInfo() ⇒
Gets relevant information about the current operating system.

**Kind**: global function  
**Returns**: - A promise that resolves with an object containing details on the current os.  
<a name="getIpConfig"></a>

## getIpConfig() ⇒
Gets details about the available network interfaces.

**Kind**: global function  
**Returns**: - A promise that resolve with the available network interfaces.  
