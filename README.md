# @dimensional-innovations/vue-electron-settings

vue-electron-settings provides utility methods for managing application settings and device information from a vue app.

## Getting Started

### Install

Add the package using yarn or npm:
```
yarn add @dimensional-innovations/vue-electron-settings
```
or
```
npm install --save @dimensional-innovations/vue-electron-settings
```

### Setup & Usage

In your electron main file, call `initSettings` with the default config:

```js
import { initSettings } from '@dimensional-innovations/vue-electron-settings/dist/main';
import { app } from 'electron';
import { config } from '../package.json';

(async function init() {
  await app.whenReady();

  const { baseApiUrl } = await initSettings(config);
  console.log(baseApiUrl);
})();
```

Then in the renderer process, you can load or modify these settings at any point with `getAppConfig` and `setAppConfig`

```js
import { getAppConfig } from '@dimensional-innovations/vue-electron-settings';

(async function run() {
  const config = await getAppConfig();
  console.log(config);

  await setAppConfig({
    baseApiUrl: config.baseApiUrl,
    applicationId: '1234'
  });
})();
```

### Plugin

While you can load settings anywhere in your application with `getAppConfig`, it can lead to a log of boilerplate since the method is async. Instead, you can also load these settings once,
and provide them to components in your app with the built-in Vue Plugin.

To setup this plugin, call createSettings with the settings you want to use.
```js
import { getAppConfig, createSettings } from '@dimensional-innovations/vue-electron-settings';
import { createApp } from 'vue';
import App from './App.vue';

(async function run() {
  const appConfig = await getAppConfig();

  createApp(App)
    .use(createSettings(appConfig))
    .mount('#app');
})();
```

These settings are then available in components. For components using the options api, they are available through the `$settings` property. For components using the composition api, `useSettings` can be called in the setup function to access them.
```js
import { defineComponent } from 'vue';

export default defineComponent({
  data: () => ({
    baseApiUrl: this.$settings.baseApiUrl
  })
});
```
```js
import { defineComponent } from 'vue';

export default defineComponent({
  setup() {
    const settings = useSettings();

    return {
      baseApiUrl: settings.baseApiUrl
    };
  }
});
```

## API 
This plugin also exposes additional information about the system the app runs on. For full list of methods available see the [API]() doc. 